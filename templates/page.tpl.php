<?php
/**
 * This template is bypassed by Panels Everywhere. It's only here as a gentle
 * fallback.
 */
?>
<?php print render($page['content']) ?>
