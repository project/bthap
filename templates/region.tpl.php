<?php
/**
 * This template is bypassed by Panels Everywhere. It's only here as a gentle
 * fallback.
 */
?>
<section class="<?php print $classes; ?>">
  <?php print $content; ?>
</section>
